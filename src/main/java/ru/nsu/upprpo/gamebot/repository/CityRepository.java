package ru.nsu.upprpo.gamebot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.upprpo.gamebot.model.City;

public interface CityRepository extends CrudRepository<City, Long> { }
