package ru.nsu.upprpo.gamebot.commands;

import ru.nsu.upprpo.gamebot.core.SessionsManager;

public class StartRockPaperScissorsGameCommand extends StartGameCommand {
    public StartRockPaperScissorsGameCommand(SessionsManager sessionsManager) {
        super(sessionsManager);
    }
}
