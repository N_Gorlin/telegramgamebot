/**
 * Непосредственно сам Бот, принимающий обновления в чате и выполняющий необходимые команды
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.nsu.upprpo.gamebot.commands.BotCommand;
import ru.nsu.upprpo.gamebot.commands.additional.BotCommandsFactory;
import ru.nsu.upprpo.gamebot.config.BotConfig;
import ru.nsu.upprpo.gamebot.core.SessionsManager;
import ru.nsu.upprpo.gamebot.exceptions.GameBotException;

@Component
@Slf4j
public class GameBot extends TelegramLongPollingBot {
    private final BotConfig config;
    private final BotCommandsFactory commandsFactory;
    private final SessionsManager sessionsManager;

    @Autowired
    public GameBot(BotConfig config, SessionsManager sessionsManager, BotCommandsFactory commandsFactory) {
        this.config = config;

        this.commandsFactory = commandsFactory;
        this.sessionsManager = sessionsManager;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String commandName = update.getMessage().getText();
            BotCommand command = this.commandsFactory.recognizeCommand(commandName);

            BotApiMethod method;

            if (command == null) {
                long chatId = update.getMessage().getChatId();

                try {
                    SendMessage answer = sessionsManager.handleUserAction(update.getMessage());
                    method = answer
                            .setChatId(chatId);
                }
                catch (GameBotException ex) {
                    if (log.isWarnEnabled()) {
                        log.warn(ex.getMessage());
                    }

                    method = new SendMessage()
                            .setChatId(chatId)
                            .setText(ex.getMessage());
                }
            } else {
                method = command.execute(update);
            }

            try {
                this.execute(method);
            } catch (TelegramApiException exception) {
                if (log.isWarnEnabled()) {
                    log.warn(exception.toString() + " " + exception.getMessage());
                }
            }
        }
    }

    @Override
    public String getBotUsername() {
        return this.config.getUsername();
    }

    @Override
    public String getBotToken() {
        return this.config.getToken();
    }
}
