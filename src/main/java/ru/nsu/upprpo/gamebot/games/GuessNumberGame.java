/**
 * Игра "Угадайка"
 *
 * Суть заключается в том, чтобы угадать число, загаданное Ботом за N попыток
 * При неправильном ответе Бот будет подсказывать, в какую сторону мыслить :)
 *
 * @author (Max Vakhrushev)
 */

package ru.nsu.upprpo.gamebot.games;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.nsu.upprpo.gamebot.games.additional.GameStatus;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class GuessNumberGame implements Game {
    private final int attemptsCount = 3;
    private final int minNumber = 1;
    private final int maxNumber = 100;

    private static SecureRandom random = new SecureRandom();

    private GameStatus status;
    private int attemptsNumber;
    private int guessedNumber;

    public GuessNumberGame()
    {
        status = GameStatus.Started;
        attemptsNumber = 0;
        guessedNumber = random.nextInt((maxNumber - minNumber) + 1) + minNumber; // [min ... max]
    }

    @Override
    public SendMessage startGame() {
        String message = String.format("Я загадал число от %d до %d.%nУ тебя %d попытки. Удачи ;)", minNumber, maxNumber, attemptsCount);

        return new SendMessage()
                .setReplyMarkup(getKeyboard())
                .setText(message);
    }

    @Override
    public SendMessage handlePlayerAction(String action) {
        SendMessage answer = new SendMessage();

        int userNumber;
        try {
            userNumber = Integer.parseInt(action);
        }
        catch (NumberFormatException exc) {
            return answer
                    .setText(String.format("Необходимо ввести число от %d до %d.", minNumber, maxNumber));
        }

        if (userNumber < minNumber) {
            return answer
                    .setText(String.format("Число должно быть больше или равно %d", minNumber));
        }

        if (userNumber > maxNumber) {
            return answer
                    .setText(String.format("Число должно быть меньше или равно %d", maxNumber));
        }

        attemptsNumber += 1;

        if (userNumber == guessedNumber) {
            status = GameStatus.Finished;
            return answer
                    .setText("Верно!");
        }

        if (attemptsNumber >= attemptsCount) {
            status = GameStatus.Finished;
            return answer
                    .setText(String.format("Попытки закончились ;(%nПобеда за мной!%nЯ загадал: %d", guessedNumber));
        }

        if (userNumber > guessedNumber) {
            return answer
                    .setText("Загаданное число меньше...");
        }

        return answer
                .setText("Загаданное число больше...");
    }

    @Override
    public String printHelp() {
        return String.format("Я загадал случайное число от %d до %d.%nВаша задача - отгадать это число!%nНе бойся: я буду подсказывать :)", minNumber, maxNumber);
    }

    @Override
    public GameStatus getStatus() {
        return status;
    }

    private ReplyKeyboard getKeyboard()
    {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        KeyboardRow row = new KeyboardRow();
        row.add(new KeyboardButton("Завершить"));

        rows.add(row);

        return keyboard;
    }
}
