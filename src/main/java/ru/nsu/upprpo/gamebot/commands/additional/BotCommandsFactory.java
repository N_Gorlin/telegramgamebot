/**
 * Фабрика для создания команд, соответствующих тем, что выбрал пользователь
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.commands.additional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.upprpo.gamebot.commands.*;
import ru.nsu.upprpo.gamebot.core.SessionsManager;

@Service
public class BotCommandsFactory {
    private SessionsManager sessionsManager;

    @Autowired
    public BotCommandsFactory(SessionsManager sessionsManager) {
        this.sessionsManager = sessionsManager;
    }


    public BotCommand recognizeCommand(String command) {
        if (command.equals(CommandsList.Start.commandName)) {
            return new StartCommand();
        }

        if (command.equals(CommandsList.Menu.commandName)) {
            return new MenuCommand();
        }

        if (command.equals(CommandsList.GetGames.commandName)) {
            return new GetGamesCommand();
        }

        if (command.equals(CommandsList.FinishGame.commandName)) {
            return new FinishGameCommand(this.sessionsManager);
        }

        if (command.equals(CommandsList.Help.commandName)) {
            return new HelpCommand();
        }

        if (command.equals(CommandsList.StartGuessNumberGame.commandName)) {
            return new StartGuessNumberGameCommand(this.sessionsManager);
        }

        if (command.equals(CommandsList.StartRockPaperScissorsGame.commandName)) {
            return new StartRockPaperScissorsGameCommand(this.sessionsManager);
        }

        if (command.equals(CommandsList.StartCitiesGame.commandName)) {
            return new StartCitiesGameCommand(this.sessionsManager);
        }

        if (command.equals(CommandsList.StartRiddleGame.commandName)) {
            return new StartRiddleGameCommand(this.sessionsManager);
        }

        return null;
    }
}
