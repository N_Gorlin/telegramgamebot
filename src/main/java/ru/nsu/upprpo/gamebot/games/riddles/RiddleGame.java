package ru.nsu.upprpo.gamebot.games.riddles;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.nsu.upprpo.gamebot.games.Game;
import ru.nsu.upprpo.gamebot.games.additional.GameStatus;

import java.util.ArrayList;
import java.util.List;

public class RiddleGame implements Game {
    private enum State {QUESTION_ASKED, DEFAULT, TYPE_AWAIT, DIFFICULTY_AWAIT, CATEGORY_AWAIT}

    private State currentState = State.DEFAULT;

    private ReplyKeyboard currentKeyboard = getDefaultKeyboard();

    private GameStatus status;

    private RiddleService riddleService = new RiddleService();

    @Override
    public SendMessage startGame() {
        status = GameStatus.Started;
        return new SendMessage().setText("Игра в загадки началась! Напиши Справка, чтобы получить правила:)")
                .setReplyMarkup(currentKeyboard);
    }

    @Override
    public SendMessage handlePlayerAction(String action) {
        String result = "";
        if (currentState == State.DEFAULT) {
            switch (action) {
                case "Справка":
                    result = printHelp();
                    break;
                case "Категории":
                    result = printAllCategories();
                    break;
                case "Типы":
                    result = printAllTypes();
                    break;
                case "Сложности":
                    result = printAllDifficulties();
                    break;
                case "Категория":
                    currentState = State.CATEGORY_AWAIT;
                    currentKeyboard = getCategoryKeyboard();
                    result = "Выбери категорию!";
                    break;
                case "Тип":
                    result = "Выбери тип!";
                    currentState = State.TYPE_AWAIT;
                    currentKeyboard= getTypeKeyboard();
                    break;
                case "Сложность":
                    result = "Выбери сложность!";
                    currentState = State.DIFFICULTY_AWAIT;
                    currentKeyboard = getDifficultyKeyboard();
                    break;
                case "Параметры":
                    result = printCurrentSettings();
                    break;
                case "Загадка":
                    result = printRiddle();
                    currentState = State.QUESTION_ASKED;
                    currentKeyboard = getAnswerKeyboard();
                    break;
                default:
                    result = "Неизвестная команда! Напиши Справка, чтобы увидеть доступные команды";
            }
        } else if (currentState == State.QUESTION_ASKED) {
            switch (action) {
                case "Пропустить":
                    result = printCorrectAnswer();
                    currentState = State.DEFAULT;
                    currentKeyboard = getDefaultKeyboard();
                    break;
                default:
                    result = checkAnswer(action);
            }
        } else if (currentState == State.DIFFICULTY_AWAIT) {
            result = setDifficulty(action);
        } else if (currentState == State.TYPE_AWAIT) {
            result = setType(action);
        } else if (currentState == State.CATEGORY_AWAIT) {
            result = setCategory(action);
        }

        return new SendMessage().setText(result).setReplyMarkup(currentKeyboard);
    }

    @Override
    public String printHelp() {
        return "Сможешь ли ты решить мои загадки?\n" +
                "Выбери категорию, уровень сложности и тип вопроса, затем я тебе дам загадку:)\n" +
                "Доступные команды:\n" +
                "Категории, чтобы увидеть доступные категории\n" +
                "Типы, чтобы увидеть доступные типы вопросов\n" +
                "Сложности, чтобы увидеть доступные уровни сложности\n" +
                "Категория <название_категории>, чтобы установить категорию на <название_категории> (То же самое для Тип <название_типа> и Сложность <название_сложности>)\n" +
                "Параметры, чтобы увидеть текущую категорию, уровень сложности и тип вопроса (По умолчанию стоят любая категория, сложность и тип\n" +
                "Загадка, чтобы получить загадку\n" +
                "Пропустить, чтобы сдаться и получить правильный ответ (Доступно только после получения загадки)";
    }

    @Override
    public GameStatus getStatus() {
        return status;
    }

    private String printAllCategories() {
        List<String> categories = riddleService.getCategories();

        if (!categories.isEmpty())
            return "Доступные категории:\n" + String.join("\n", categories);
        else
            return "Произошла ошибка во время получения категорий.";
    }

    private String printAllTypes() {
        return "Доступные типы вопросов:\n" + String.join("\n", riddleService.getTypes());
    }

    private String printAllDifficulties() {
        return "Доступные уровни сложности:\n" + String.join("\n", riddleService.getDifficulties());
    }

    private String setCategory(String category) {
        if (riddleService.setCategory(category)) {
            currentState = State.DEFAULT;
            currentKeyboard = getDefaultKeyboard();
            return "Категория установелна на " + category;
        } else {
            return "Нет такой категории, напиши категории, чтобы увидеть доступные опции";
        }
    }

    private String setType(String type) {
        if (riddleService.setType(type)) {
            currentState = State.DEFAULT;
            currentKeyboard = getDefaultKeyboard();
            return "Тип установлен на " + type;
        } else {
            return "Нет такого типа, напиши типы, чтобы увидеть доступные опции";
        }
    }

    private String setDifficulty(String difficulty) {
        if (riddleService.setDifficulty(difficulty)) {
            currentState = State.DEFAULT;
            currentKeyboard = getDefaultKeyboard();
            return "Сложность установлена на " + difficulty;
        } else {
            return "Нет такой сложности, напиши сложности, чтобы увидеть доступные опции";
        }
    }

    private String printCurrentSettings() {
        return "Текущие параметры: " + riddleService.getCurrentSettings();
    }

    private String printRiddle() {
        return riddleService.getRiddle();
    }

    private String printCorrectAnswer() {
        return "Правильный ответ " + riddleService.getCorrectAnswer();
    }

    private String checkAnswer(String answer) {
        if (riddleService.isCorrectAnswer(answer)) {
            currentState = State.DEFAULT;
            currentKeyboard = getDefaultKeyboard();
            return "Правильно!";
        } else {
            return "Неправильно, попробуй еще раз!";
        }
    }

    private ReplyKeyboard getDefaultKeyboard() {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        String[] firstGroup = new String[] { "Загадка", "Параметры", "Справка"};
        String[] secondGroup = new String[] {"Категории", "Типы", "Сложности" };
        String[] thirdGroup = new String[] {"Тип", "Сложность", "Категория"};

        KeyboardRow firstRow = new KeyboardRow();
        for (String button: firstGroup) {
            firstRow.add(button);
        }

        KeyboardRow secondRow = new KeyboardRow();
        for (String button: secondGroup) {
            secondRow.add(button);
        }

        KeyboardRow thirdRow = new KeyboardRow();
        for (String button: thirdGroup) {
            thirdRow.add(button);
        }

        KeyboardRow lastRow = new KeyboardRow();
        lastRow.add("Завершить");

        rows.add(firstRow);
        rows.add(secondRow);
        rows.add(thirdRow);
        rows.add(lastRow);

        return keyboard;
    }

    private ReplyKeyboard getAnswerKeyboard() {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        ArrayList<String> allAnswers = riddleService.getAllAnswers();
        KeyboardRow answerRow = new KeyboardRow();
        allAnswers.forEach(answerRow::add);

        KeyboardRow optionRow = new KeyboardRow();
        optionRow.add("Пропустить");

        rows.add(answerRow);
        rows.add(optionRow);

        return keyboard;
    }

    private ReplyKeyboard getTypeKeyboard() {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        String[] typeButtons = new String[] { "Несколько вариантов", "Правда/Ложь"};
        KeyboardRow typesRow = new KeyboardRow();
        for (String button: typeButtons) {
            typesRow.add(button);
        }
        rows.add(typesRow);

        return keyboard;
    }

    private ReplyKeyboard getDifficultyKeyboard() {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        String[] difficultyButtons = new String[] { "Легко", "Нормально", "Сложно" };
        KeyboardRow difficultyRow = new KeyboardRow();
        for (String button: difficultyButtons) {
            difficultyRow.add(button);
        }
        rows.add(difficultyRow);

        return keyboard;
    }

    private ReplyKeyboard getCategoryKeyboard() {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);
        List<String> allCategories = riddleService.getCategories();

        for (String category : allCategories) {
            KeyboardRow currentRow = new KeyboardRow();
            currentRow.add(category);
            rows.add(currentRow);
        }

        return keyboard;
    }
}

