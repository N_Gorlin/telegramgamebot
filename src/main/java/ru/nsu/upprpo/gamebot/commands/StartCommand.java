/**
 * Команда для иницализации Бота
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.commands;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class StartCommand implements BotCommand {
    @Override
    public BotApiMethod execute(Update update) {
        String userFirstName = update.getMessage().getFrom().getFirstName();
        String answer = String.format("Привет, %s!%n%nЧтобы управлять мной, используй меню снизу ;)", userFirstName);

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setReplyMarkup(getKeyboard())
                .setText(answer);
    }

    public static ReplyKeyboardMarkup getKeyboard()
    {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        KeyboardRow firstRow = new KeyboardRow();
        firstRow.add(new KeyboardButton("Игры"));

        KeyboardRow secondRow = new KeyboardRow();
        secondRow.add(new KeyboardButton("Помощь"));

        rows.add(firstRow);
        rows.add(secondRow);

        return keyboard;
    }
}
