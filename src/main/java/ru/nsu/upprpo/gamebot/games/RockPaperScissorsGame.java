/**
 * Игра "Камень-ножницы-бумага"
 *
 * Всем известная игра про камень, ножницы и бумагу
 *
 * @author (Ilya Ponomarenko)
 */

package ru.nsu.upprpo.gamebot.games;

import org.springframework.lang.Nullable;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.nsu.upprpo.gamebot.games.additional.GameStatus;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RockPaperScissorsGame implements Game{
    enum Move {
        Rock("Камень", 0),
        Paper("Бумага", 1),
        Scissors("Ножницы", 2);

        private final String name;
        private final int intValue;

        Move(String name, int intValue) {
            this.name = name;
            this.intValue = intValue;
        }

        @Nullable
        public static Move fromString(String move) {
            Move answer = null;

            for (Move m : Move.values()) {
                if (m.name.equals(move)) {
                    answer = m;
                    break;
                }
            }

            return answer;
        }

        // -1 if other is larger, 1 if other is smaller, 0 if equal
        public int compare(Move other) {
            if (other.intValue == this.intValue) {
                return 0;
            } else if ((this.intValue + 1) % 3 == other.intValue) {
                return -1;
            } else {
                return 1;
            }

        }
    }

    private SecureRandom random = new SecureRandom();

    private int botScore = 0;

    private int userScore = 0;

    private GameStatus status;

    @Override
    public SendMessage startGame() {
        status = GameStatus.Started;
        return new SendMessage()
                .setReplyMarkup(this.getKeyboard())
                .setText(String.format("Камень-ножницы-бумага!%nРаз.. Два.. Три!"));
    }

    @Override
    public SendMessage handlePlayerAction(String action) {
        String result;
        switch (action) {
            case "Справка":
                result = printHelp();
                break;
            case "Счет":
                result = printScore();
                break;
            case "Камень":
            case "Бумага":
            case "Ножницы":
                result = processMove(Move.fromString(action));
                break;
            default:
                result = "Неизвестный ход ;(";
        }

        return new SendMessage()
                .setText(result);
    }

    private String processMove(Move userMove) {
        Move botMove = Move.values()[random.nextInt(3)];
        String result = "Я выбрал: " + botMove.name + "\n";

        switch (userMove.compare(botMove)) {
            case -1:
                result += "Я победил ;)";
                botScore++;
                break;
            case 1:
                result += "Ты победил! Оо";
                userScore++;
                break;
            case 0:
                result += "Ничья -_-";
                userScore++;
                botScore++;
        }

        return result;
    }

    private String printScore() {
        return "Я: " + botScore + " Ты: " + userScore;
    }

    public String printHelp() {
        return String.format("Старые добрые камень-ножницы-бумага!%nкамень > ножницы > бумага > камень");
    }

    @Override
    public GameStatus getStatus() {
        return status;
    }

    private ReplyKeyboard getKeyboard()
    {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        String[] moveButtons = new String[] { "Камень", "Ножницы", "Бумага"};
        String[] funcButtons = new String[] { "Справка", "Счет", "Завершить" };

        KeyboardRow movesRow = new KeyboardRow();
        for (String button: moveButtons) {
            movesRow.add(button);
        }

        KeyboardRow funcRow = new KeyboardRow();
        for (String button: funcButtons) {
            funcRow.add(button);
        }

        rows.add(movesRow);
        rows.add(funcRow);

        return keyboard;
    }
}
