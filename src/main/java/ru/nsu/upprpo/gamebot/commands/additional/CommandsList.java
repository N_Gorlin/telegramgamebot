/**
 * Перечисление для всех команд, которыми будет оперировать Бот
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.commands.additional;

public enum CommandsList {
    Start("/start", "Запускает Бота"),
    Menu("Меню", "Выводит начальное меню"),
    GetGames("Игры", "Показать список доступных игр"),
    FinishGame("Завершить", "Завершить текущую игру"),
    Help("Помощь", "Выводит справочную информацию о Боте"),

    // Games
    StartGuessNumberGame("Угадайка", "Запускает игру 'Угадайка'"),
    StartRockPaperScissorsGame("Камень-ножницы-бумага", "Запускает игру 'Камень-ножницы-бумага'"),
    StartCitiesGame("Города", "Запускает игру 'Города'"),
    StartRiddleGame("Загадки", "Запускает игру в загадки");

    public final String commandName;
    public final String commandDescription;

    CommandsList(String commandName, String commandDescription) {
        this.commandName = commandName;
        this.commandDescription = commandDescription;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", commandName, commandDescription);
    }
}
