/**
 * Фабрика для создания игр, соответсвующих запросу пользователя
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.games.additional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import ru.nsu.upprpo.gamebot.games.Game;
import ru.nsu.upprpo.gamebot.games.GuessNumberGame;
import ru.nsu.upprpo.gamebot.games.RockPaperScissorsGame;
import ru.nsu.upprpo.gamebot.games.cities.CitiesGame;
import ru.nsu.upprpo.gamebot.games.riddles.RiddleGame;
import ru.nsu.upprpo.gamebot.games.cities.CitiesData;

@Service
public class GamesFactory {
    private CitiesData data;

    @Autowired
    public GamesFactory(CitiesData data) {
        this.data = data;
    }

    @Nullable
    public Game createGame(String name) {
        if (name.equals(GamesList.GuessNumber.gameName)) {
            return new GuessNumberGame();
        }

        if (name.equals(GamesList.RockPaperScissors.gameName)) {
            return new RockPaperScissorsGame();
        }

        if (name.equals(GamesList.Riddles.gameName)) {
            return new RiddleGame();
        }

        if (name.equals(GamesList.Cities.gameName)) {
            return new CitiesGame(data);
        }

        return null;
    }
}
