package ru.nsu.upprpo.gamebot.games.cities;

import com.google.common.collect.Streams;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.nsu.upprpo.gamebot.games.Game;
import ru.nsu.upprpo.gamebot.games.additional.GameStatus;
import ru.nsu.upprpo.gamebot.model.City;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class CitiesGame implements Game {
    private CitiesData data;
    private HashSet<String> alreadyUsedCities;
    private GameStatus status;

    private int score;
    private String previousCity;

    public CitiesGame(CitiesData data) {
        this.data = data;

        alreadyUsedCities = new HashSet<>();
        status = GameStatus.Started;
        score = 0;
    }

    @Override
    public SendMessage startGame() {
        String message = String.format("Начнём игру! Мой ход первый!%n%n%s", this.nameRandomCity(data.getAllCities()));

        return new SendMessage()
                .setReplyMarkup(getKeyboard())
                .setText(message);
    }

    @Override
    public SendMessage handlePlayerAction(String action) {
        String result;

        switch (action) {
            case "Счет":
                result = this.getResult();
                break;
            default:
                result = handleCity(action);
                break;
        }

        return new SendMessage()
                .setText(result);
    }

    @Override
    public String printHelp() {
        return "Твоя задача - назвать как можно городов, начинающихся с последней буквы города, который назову я! Удачи!";
    }

    @Override
    public GameStatus getStatus() {
        return this.status;
    }

    private String createAnswerMessage(String cityName) {
        return String.format("%s%nТебе на %s!", cityName, cityName.substring(cityName.length() - 1));
    }

    private String getResult() {
        return String.format("Ты держишься уже %d раундов!", score);
    }

    private String handleCity(String cityName) {
        if (alreadyUsedCities.contains(cityName)) {
            return "Этот город уже был в игре!";
        }

        if (!this.data.hasCityWithName(cityName)) {
            return "Такого города нет в моей базе!";
        }

        String lastLetterOld = this.previousCity.substring(previousCity.length() - 1).toLowerCase();
        String firstLetterNew = cityName.substring(0, 1).toLowerCase();

        if (!lastLetterOld.equals(firstLetterNew)) {
            return "Название моего города начинается на другую букву!";
        }

        score++;

        String lastLetterNew = cityName.substring(cityName.length() - 1);

        List<City> availableCities = Streams
                .stream(data.getAllCities())
                .filter(c -> !alreadyUsedCities.contains(c.getName()) && c.getName().toLowerCase().startsWith(lastLetterNew))
                .collect(Collectors.toList());

        return nameRandomCity(availableCities);
    }

    private String nameRandomCity(List<City> cities) {
        SecureRandom random = new SecureRandom();

        int cityNumber = random.nextInt(cities.size() - 1);
        String cityName = cities.get(cityNumber).getName();

        alreadyUsedCities.add(cityName);
        previousCity = cityName;

        return this.createAnswerMessage(cityName);
    }

    private ReplyKeyboardMarkup getKeyboard() {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        String[] buttonsNames = new String[] { "Счет", "Завершить" };

        for (String buttonName: buttonsNames) {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton(buttonName));

            rows.add(row);
        }

        return keyboard;
    }
}
