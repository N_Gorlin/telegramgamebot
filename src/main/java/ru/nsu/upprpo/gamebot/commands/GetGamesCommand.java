/**
 * Команда для получения всех доступных в настоящий момент игр
 *
 * @author (Max Vakhrushev)
 */

package ru.nsu.upprpo.gamebot.commands;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.nsu.upprpo.gamebot.games.additional.GamesList;

import java.util.ArrayList;
import java.util.List;

public class GetGamesCommand implements BotCommand {
    @Override
    public BotApiMethod execute(Update update) {
        String message = "Выбери игру, в которую хочешь сыграть:";

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setReplyMarkup(getKeyboard())
                .setText(message);
    }

    public static ReplyKeyboard getKeyboard()
    {
        List<KeyboardRow> rows = new ArrayList<>();
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(rows);

        for (GamesList game : GamesList.values()) {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton(game.gameName));

            rows.add(row);
        }

        KeyboardRow lastRow = new KeyboardRow();
        lastRow.add(new KeyboardButton("Меню"));
        rows.add(lastRow);

        return keyboard;
    }
}
