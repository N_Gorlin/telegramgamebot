/**
 * Исключение, возникающе при получить несуществующую игровую сессию
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.exceptions;

public class SessionNotFoundException extends GameBotException {
    public SessionNotFoundException() {
        super();
    }

    public SessionNotFoundException(String message) {
        super(message);
    }

    public SessionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public SessionNotFoundException(Throwable cause) {
        super(cause);
    }

    protected SessionNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
