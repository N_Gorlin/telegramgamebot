/**
 * Перечисление всех игр, в которые будет способен играть Бот
 *
 * @author (Max Vakhrushev)
 */

package ru.nsu.upprpo.gamebot.games.additional;

public enum GamesList {
    Cities("Города", "Называйте город на последнюю букву предыдущего!"),
    GuessNumber("Угадайка", "Попробуй угадать число, которое я загадал!"),
    RockPaperScissors("Камень-ножницы-бумага", "Всем известная игра про камень, бумагу и ножницы."),
    Riddles("Загадки", "Игра в загадки");

    public final String gameName;
    public final String gameDescription;

    GamesList(String gameName, String gameDescription) {
        this.gameName = gameName;
        this.gameDescription = gameDescription;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", gameName, gameDescription);
    }
}
