package ru.nsu.upprpo.gamebot.games.riddles;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
class RiddleService {
    private final RestTemplate restTemplate = new RestTemplate();

    private final String apiURL = "https://opentdb.com/api.php";

    private final String categoriesUrlString = "https://opentdb.com/api_category.php";

    private Map<String, Integer> categoryMap;

    private final Map<String, String> difficulties = Stream.of(
            new AbstractMap.SimpleEntry<>("Легко", "easy"),
            new AbstractMap.SimpleEntry<>("Нормально", "medium"),
            new AbstractMap.SimpleEntry<>("Сложно", "hard"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private final Map<String, String> types = Stream.of(
            new AbstractMap.SimpleEntry<>("Несколько вариантов", "multiple"),
            new AbstractMap.SimpleEntry<>("Правда/Ложь", "boolean"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


    // 0 is Any category
    private int currentCategoryId = 0;

    private String currentCategory = "Any";

    private String currentDifficulty = "Any";

    private String currentType = "Any";

    private String correctAnswer;

    private ArrayList<String> allAnswers;

    RiddleService() {
        String result = restTemplate.getForObject(categoriesUrlString, String.class);
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {};
        Map<String, Object> data = null;
        try {
            data = mapper.readValue(result, typeRef);
        } catch (IOException e) {
            if (log.isWarnEnabled()) {
                log.warn(e.getMessage());
            }
        }
        if (data != null) {
            List<Map<String, Object>> categories = (List<Map<String, Object>>) data.get("trivia_categories");
            categoryMap = categories.stream().collect(Collectors.toMap(c -> (String) c.get("name"), c -> (Integer) c.get("id")));
        } else {
            categoryMap = new HashMap<>();
        }
    }

    List<String> getCategories() {
        return new ArrayList<>(categoryMap.keySet());
    }

    List<String> getTypes() {
        return new ArrayList<>(types.keySet());
    }

    List<String> getDifficulties() {
        return new ArrayList<>(difficulties.keySet());
    }

    boolean setCategory(String category) {
        if (categoryMap.containsKey(category)) {
            currentCategoryId = categoryMap.get(category);
            currentCategory = category;
            return true;
        } else {
            return false;
        }
    }

    boolean setType(String type) {
        if (types.containsKey(type)) {
            currentType = types.get(type);
            return true;
        } else {
            return false;
        }
    }

    boolean setDifficulty(String difficulty) {
        if (difficulties.containsKey(difficulty)) {
            currentDifficulty = difficulties.get(difficulty);
            return true;
        } else {
            return false;
        }
    }

    String getCurrentSettings() {
        return "категория: " + currentCategory + ", тип: " + currentType + ", сложность: " + currentDifficulty;
    }

    String getRiddle() {
        String riddleString = null;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(apiURL).queryParam("amount", 1);
        if (currentCategoryId != 0)
            builder.queryParam("category", currentCategoryId);
        if (!currentType.equals("Any"))
            builder.queryParam("type", currentType);
        if (!currentDifficulty.equals("Any"))
            builder.queryParam("difficulty", currentDifficulty);
        String result = restTemplate.getForObject(builder.build().toString(), String.class);
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {};
        Map<String, Object> data = null;
        try {
            data = mapper.readValue(result, typeRef);
        } catch (IOException e) {
            if (log.isWarnEnabled()) {
                log.warn(e.getMessage());
            }
        }

        if (data != null) {
            Map<String, Object> riddle = ((List<Map<String, Object>>) data.get("results")).get(0);
            String question = StringEscapeUtils.unescapeHtml4((String) riddle.get("question"));
            correctAnswer = StringEscapeUtils.unescapeHtml4((String) riddle.get("correct_answer"));
            allAnswers = (ArrayList<String>) riddle.get("incorrect_answers");
            allAnswers.add(correctAnswer);
            allAnswers = (ArrayList<String>) allAnswers.stream().map(StringEscapeUtils::unescapeHtml4).collect(Collectors.toList());
            Collections.shuffle(allAnswers);
            riddleString = question + "\n" + String.join("\n", allAnswers);
        }

        return riddleString;
    }

    String getCorrectAnswer() {
        return correctAnswer;
    }

    ArrayList<String> getAllAnswers() {
        return new ArrayList<>(allAnswers);
    }

    boolean isCorrectAnswer(String answer) {
        return correctAnswer.equalsIgnoreCase(answer);
    }
}
