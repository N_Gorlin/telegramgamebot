/**
 * Интерфейс для всех игр, в которые будет уметь играть Бот
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.games;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.nsu.upprpo.gamebot.games.additional.GameStatus;

public interface Game {
    SendMessage startGame();

    SendMessage handlePlayerAction(String action);

    String printHelp();

    GameStatus getStatus();
}
