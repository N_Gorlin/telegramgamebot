/**
 * Команда для получения справочной информации о Боте
 *
 * @author (Max Vakhrushev)
 */

package ru.nsu.upprpo.gamebot.commands;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public class HelpCommand implements BotCommand {
    @Override
    public BotApiMethod execute(Update update) {
        String message = String.format("Со мной ты можешь сыграть в различные игры :)%n%nЧтобы посмотреть доступные игры - нажми на кнопку 'Игры'%n%nВсё просто ;)");

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText(message);
    }
}
