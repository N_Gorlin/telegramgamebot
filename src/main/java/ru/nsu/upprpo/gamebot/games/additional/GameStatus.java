/**
 * Класс с состояниями игровой сессии
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.games.additional;

public enum GameStatus {
    Started,
    Finished
}
