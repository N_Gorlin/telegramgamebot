/**
 * Интерфейс для будущих комманд, которые будет исполнять Бот
 *
 * @author (Nikia Gorlin)
 */

package ru.nsu.upprpo.gamebot.commands;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface BotCommand {
    BotApiMethod execute(Update update);
}
