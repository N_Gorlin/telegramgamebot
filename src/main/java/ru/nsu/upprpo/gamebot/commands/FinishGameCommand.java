/**
 * Команда для завершения текущей сессии игры с пользователем
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.commands;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.nsu.upprpo.gamebot.core.SessionsManager;
import ru.nsu.upprpo.gamebot.exceptions.GameBotException;

@Slf4j
public class FinishGameCommand implements BotCommand {
    private SessionsManager sessionsManager;

    public FinishGameCommand(SessionsManager sessionsManager) {
        this.sessionsManager = sessionsManager;
    }

    @Override
    public BotApiMethod execute(Update update) {
        int userId = update.getMessage().getFrom().getId();

        String answer;

        try {
            this.sessionsManager.finishSession(userId);
            answer = String.format("Игра завершена...%n%nНе хочешь ли начать заново или сыграть в другую? ;)");
        } catch (GameBotException e) {
            if (log.isWarnEnabled()) {
                log.warn(e.toString() + " " + e.getMessage());
            }
            answer = "Игровая сессия уже была завершена!";
        }

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setReplyMarkup(GetGamesCommand.getKeyboard())
                .setText(answer);
    }
}
