/**
 * Исключение, возникающе при попытке начать ту же игру, что уже запущена
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.exceptions;

public class AlreadyExistedSessionException extends GameBotException {
    public AlreadyExistedSessionException() {
        super();
    }

    public AlreadyExistedSessionException(String message) {
        super(message);
    }

    public AlreadyExistedSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyExistedSessionException(Throwable cause) {
        super(cause);
    }

    protected AlreadyExistedSessionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
