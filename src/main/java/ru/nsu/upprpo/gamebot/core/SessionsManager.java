/**
 * Класс-менеджер для управления игровыми сессиями с пользователями в реальном времени
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.nsu.upprpo.gamebot.exceptions.AlreadyExistedSessionException;
import ru.nsu.upprpo.gamebot.exceptions.GameBotException;
import ru.nsu.upprpo.gamebot.exceptions.GameNotFoundException;
import ru.nsu.upprpo.gamebot.games.Game;
import ru.nsu.upprpo.gamebot.games.additional.GamesFactory;
import ru.nsu.upprpo.gamebot.exceptions.SessionNotFoundException;
import java.util.HashMap;
import java.util.Map;

@Service
public class SessionsManager {
    private Map<Integer, GameSession> sessions;
    private GamesFactory factory;

    @Autowired
    public SessionsManager(GamesFactory factory) {
        this.sessions = new HashMap<>();
        this.factory = factory;
    }

    public SendMessage handleUserAction(Message message) throws GameBotException {
        int userId = message.getFrom().getId();

        GameSession current = this.sessions.get(userId);
        if (current == null) {
            throw new SessionNotFoundException("Сессия не найдена!");
        }

        SendMessage answer = current.handlePlayerAction(message.getText());
        if (current.isFinished()) {
            this.finishSession(userId);
        }

        return answer;
    }

    public SendMessage createSession(int userId, String gameName) throws GameBotException {
        if (this.sessions.containsKey(userId)) {
            throw new AlreadyExistedSessionException("You have already started game\nIf you want to start another, first finish that game");
        }

        Game game = this.factory.createGame(gameName);
        if (game == null) {
            throw new GameNotFoundException("Such game does not present");
        }

        SendMessage message = game.startGame();

        this.sessions.put(userId, new GameSession(game, userId));

        return message;
    }

    public void finishSession(int userId) throws GameBotException {
        if (!this.sessions.containsKey(userId)) {
            throw new SessionNotFoundException();
        }

        this.sessions.remove(userId);
    }
}
