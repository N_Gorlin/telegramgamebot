/**
 * Класс игровой сессии с пользователем
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.core;

import lombok.Getter;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.nsu.upprpo.gamebot.games.Game;
import ru.nsu.upprpo.gamebot.games.additional.GameStatus;

@Getter
public class GameSession {
    private Game gameInstance;

    private int playerId;

    private boolean isFinished;

    public GameSession(Game gameInstance, int userId) {
        this.gameInstance = gameInstance;
        this.playerId = userId;
    }

    public SendMessage handlePlayerAction(String action) {
        SendMessage answer = this.gameInstance.handlePlayerAction(action);

        if (this.gameInstance.getStatus().equals(GameStatus.Finished)) {
            isFinished = true;
        }

        return answer;
    }
}
