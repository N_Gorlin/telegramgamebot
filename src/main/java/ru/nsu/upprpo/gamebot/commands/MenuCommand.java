/**
 * Команда для получения вывода меню пользователю
 *
 * @author (Max Vakhrushev)
 */

package ru.nsu.upprpo.gamebot.commands;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public class MenuCommand implements BotCommand {
    @Override
    public BotApiMethod execute(Update update) {
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Меню:")
                .setReplyMarkup(StartCommand.getKeyboard());
    }
}
