package ru.nsu.upprpo.gamebot.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@Builder
@NoArgsConstructor
public class City {
    @Id
    @Column
    private Long id;

    @Column(name = "city_name")
    private String name;

    public City(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
