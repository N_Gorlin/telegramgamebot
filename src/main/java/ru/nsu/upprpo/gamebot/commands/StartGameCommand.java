/**
 * Команда для инициализации игровой сессии с пользователем
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.commands;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.nsu.upprpo.gamebot.core.SessionsManager;
import ru.nsu.upprpo.gamebot.exceptions.GameBotException;

@Slf4j
public class StartGameCommand implements BotCommand {
    private SessionsManager sessionsManager;

    public StartGameCommand(SessionsManager sessionsManager) {
        this.sessionsManager = sessionsManager;
    }

    @Override
    public BotApiMethod execute(Update update) {
        String gameName = update.getMessage().getText();
        int userId = update.getMessage().getFrom().getId();

        SendMessage answer = new SendMessage();

        try {
            answer = this.sessionsManager.createSession(userId, gameName);
        } catch (GameBotException e) {
            if (log.isWarnEnabled()) {
                log.warn(e.toString() + " " + e.getMessage());
            }
            answer.setText(e.getMessage());
        }
        
        return answer
                .setChatId(update.getMessage().getChatId());
    }
}
