/**
 * Класс для запуска приложения на Spring
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class App 
{
    public static void main( String[] args ) {
        ApiContextInitializer.init();

        ConfigurableApplicationContext ctx = SpringApplication.run(App.class, args);
        ctx.close();
    }
}
