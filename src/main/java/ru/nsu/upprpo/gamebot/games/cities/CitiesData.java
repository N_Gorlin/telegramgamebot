package ru.nsu.upprpo.gamebot.games.cities;

import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.upprpo.gamebot.model.City;
import ru.nsu.upprpo.gamebot.repository.CityRepository;

import java.util.List;

@Getter
@Service
public class CitiesData {
    private List<City> allCities;
    private CityRepository cityRepository;

    @Autowired
    public CitiesData(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
        this.allCities = Lists.newArrayList(cityRepository.findAll());
    }

    public boolean hasCityWithName(String name) {
        return Streams.stream(allCities).anyMatch(city -> city.getName().equals(name));
    }
}
