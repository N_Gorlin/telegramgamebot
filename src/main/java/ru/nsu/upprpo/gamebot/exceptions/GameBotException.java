/**
 * Любое исключение, возникающие при работе Бота
 *
 * @author (Nikita Gorlin)
 */

package ru.nsu.upprpo.gamebot.exceptions;

public class GameBotException extends Exception {
    public GameBotException() {
        super();
    }

    public GameBotException(String message) {
        super(message);
    }

    public GameBotException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameBotException(Throwable cause) {
        super(cause);
    }

    protected GameBotException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
